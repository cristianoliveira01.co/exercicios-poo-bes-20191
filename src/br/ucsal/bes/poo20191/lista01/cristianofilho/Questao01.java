package br.ucsal.bes.poo20191.lista01.cristianofilho;

import java.util.Scanner;

/*
 *@author Cristiano Filho [cristiano.filho@ucsal.edu.br]
 * @version 1.0
 */
public class Questao01 {

	public static void main(String[] args) {
		// variaveis
		String nome;
		String sobrenome;
		// processamento
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe o primeiro nome:");
		nome = sc.nextLine();
		System.out.println("Informe o  sobrenome:");
		sobrenome = sc.nextLine();
		// sa�da
		System.out.println(nome + " " + sobrenome);
	}

}
